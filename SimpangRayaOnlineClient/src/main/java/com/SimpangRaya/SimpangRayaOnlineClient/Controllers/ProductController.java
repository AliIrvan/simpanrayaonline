package com.SimpangRaya.SimpangRayaOnlineClient.Controllers;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMCategory;
import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMProduct;
import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMVariant;

@Controller
@RequestMapping("/Product")
public class ProductController {

    private final String apiProductUrl = "http://localhost:8080/apiProduct";
    private final String apiVariantUrl = "http://localhost:8080/apiVariant";
    private final String apiCategoryUrl = "http://localhost:8080/apiCategory";
    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("")
    public ModelAndView Index() {
        ModelAndView view = new ModelAndView("product/index");
        view.addObject("title", "Daftar Product");
        try {
            ResponseEntity<VMMProduct[]> apiResponse = restTemplate.getForEntity(apiProductUrl, VMMProduct[].class);
            if (apiResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Products", apiResponse.getBody());
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Create")
    public ModelAndView Create() {
        ModelAndView view = new ModelAndView("product/create");
        view.addObject("VMMProduct", new VMMProduct());
        try {
            ResponseEntity<VMMCategory[]> apiCategoryResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            if (apiCategoryResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Categories", apiCategoryResponse.getBody());
            } else {
                throw new RestClientException(apiCategoryResponse.getStatusCode().toString() + " : " + Arrays.toString(apiCategoryResponse.getBody()));
            }
            ResponseEntity<VMMVariant[]> apiVariantResponse = restTemplate.getForEntity(apiVariantUrl, VMMVariant[].class);
            if (apiVariantResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Variants", apiVariantResponse.getBody());
            } else {
                throw new RestClientException(apiVariantResponse.getStatusCode().toString() + " : " + Arrays.toString(apiVariantResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Create")
    public ResponseEntity<?> Create(VMMProduct data) {
        try {
            ResponseEntity<VMMProduct> apiResponse = restTemplate.postForEntity(apiProductUrl, data, VMMProduct.class);
            return new ResponseEntity<>(apiResponse.getBody(), apiResponse.getStatusCode());
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ModelAndView Detail(@PathVariable long id) {
        ModelAndView view = new ModelAndView("product/detail");
        try {
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiProductUrl + "/" + id, VMMProduct.class);
            if (apiResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Product", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Update/{id}")
    public ModelAndView Update(@PathVariable long id) {
        ModelAndView view = new ModelAndView("Product/update");
        try {
            ResponseEntity<VMMProduct> apiProductResponse = restTemplate.getForEntity(apiProductUrl + "/" + id, VMMProduct.class);
            if (apiProductResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Product", apiProductResponse.getBody());
            } else {
                throw new Exception(apiProductResponse.getStatusCode().toString() + ":" + apiProductResponse.getBody());
            }
            ResponseEntity<VMMVariant[]> apiVariantResponse = restTemplate.getForEntity(apiVariantUrl, VMMVariant[].class);
            if (apiVariantResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Variants", apiVariantResponse.getBody());
            } else {
                throw new Exception(apiVariantResponse.getStatusCode().toString() + ":" + Arrays.toString(apiVariantResponse.getBody()));
            }
            ResponseEntity<VMMCategory[]> apiCategoryResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            if (apiCategoryResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Categories", apiCategoryResponse.getBody());
            } else {
                throw new Exception(apiCategoryResponse.getStatusCode().toString() + ":" + Arrays.toString(apiCategoryResponse.getBody()));
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Update")
    public ResponseEntity<?> Update(VMMProduct data) {
        try {
            restTemplate.put(apiProductUrl, data);
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + data.getId(), VMMProduct.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/Delete/{id}")
    public ModelAndView Delete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("product/delete");
        try {
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiProductUrl + "/" + id, VMMProduct.class);
            if (apiResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Product", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());

            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Delete")
    public ResponseEntity<?> Delete(VMMProduct data) {
        try {
            restTemplate.delete(apiProductUrl + "/" + data.getId() + "/" + data.getUpdateBy());
            return new ResponseEntity<>("Product " + data.getName() +" berhasil dihapus", HttpStatus.OK);
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/IndexAll")
    public ModelAndView IndexAll() {
        ModelAndView view = new ModelAndView("product/index-all");
        view.addObject("title", "Product Terhapus");
        try {
            ResponseEntity<VMMProduct[]> apiResponse = restTemplate.getForEntity(apiProductUrl + "/RecycleBin", VMMProduct[].class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                VMMProduct[] data = apiResponse.getBody();
                view.addObject("Success", true);
                view.addObject("Products", data);
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Retrieve/{id}")
    public ModelAndView Retrieve(@PathVariable long id) {
        ModelAndView view = new ModelAndView("product/retrieve");
        
        try {
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiProductUrl + "/findById/" + id, VMMProduct.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Product", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Retrieve")
    public ResponseEntity<?> Retrieve(VMMProduct data) {
        try {
            restTemplate.put(apiProductUrl + "/Retrieve", data);
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiProductUrl + "/" + data.getId(), VMMProduct.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/HardDelete/{id}")
    public ModelAndView HardDelete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("product/hard-delete");
        
        try {
            ResponseEntity<VMMProduct> apiResponse = restTemplate.getForEntity(apiProductUrl + "/findById/" + id, VMMProduct.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Product", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/HardDelete")
    public ResponseEntity<?> HardDelete(VMMProduct data) {
        try {
            restTemplate.delete(apiProductUrl + "/HardDelete/" + data.getId());
            return new ResponseEntity<>("Product " + data.getName() +" berhasil dihapus", HttpStatus.OK);
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
