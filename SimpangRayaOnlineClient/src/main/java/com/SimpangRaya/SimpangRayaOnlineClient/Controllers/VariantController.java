package com.SimpangRaya.SimpangRayaOnlineClient.Controllers;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMCategory;
import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMVariant;

@Controller
@RequestMapping("/Variant")

public class VariantController {

    private final String apiVariantUrl = "http://localhost:8080/apiVariant";
    private final String apiCategoryUrl = "http://localhost:8080/apiCategory";
    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("")
    public ModelAndView Index() {
        ModelAndView view = new ModelAndView("variant/index");
        view.addObject("title", "Daftar Variant");
        try {
            ResponseEntity<VMMVariant[]> apiResponse = restTemplate.getForEntity(apiVariantUrl, VMMVariant[].class);
            if (apiResponse.getStatusCode() == HttpStatus.OK) {
                view.addObject("Success", true);
                view.addObject("Variants", apiResponse.getBody());
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }  

    @GetMapping("/Create")
    public ModelAndView Create() {
        ModelAndView view = new ModelAndView("variant/create");
        view.addObject("VMMVariant", new VMMVariant());
        try {
            ResponseEntity<VMMCategory[]> apiResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Categories", apiResponse.getBody());
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Create")
    public ResponseEntity<?> Create(VMMVariant data) {
        try {
            ResponseEntity<VMMVariant> apiResponse = restTemplate.postForEntity(apiVariantUrl, data, VMMVariant.class);
            return new ResponseEntity<>(apiResponse.getBody(), apiResponse.getStatusCode());
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ModelAndView Detail(@PathVariable long id) {
        ModelAndView view = new ModelAndView("variant/detail");
        try {
            ResponseEntity<VMMVariant> apiResponse = restTemplate.getForEntity(apiVariantUrl + "/" + id, VMMVariant.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Variant", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Update/{id}")
    public ModelAndView Update(@PathVariable long id) {
        ModelAndView view = new ModelAndView("variant/update");
        try {
            ResponseEntity<VMMVariant> apiVariantResponse = restTemplate.getForEntity(apiVariantUrl + "/" + id, VMMVariant.class);
            ResponseEntity<VMMCategory[]> apiCategoryResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            if(apiVariantResponse.getStatusCode() == HttpStatus.OK && apiCategoryResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Variant", apiVariantResponse.getBody());
                view.addObject("Categories", apiCategoryResponse.getBody());
            } else {
                if(apiVariantResponse.getStatusCode() != HttpStatus.OK){
                    throw new Exception(apiVariantResponse.getStatusCode().toString() + ":" + apiVariantResponse.getBody());
                } else {
                    throw new Exception(apiCategoryResponse.getStatusCode().toString() + ":" + Arrays.toString(apiCategoryResponse.getBody()));
                }
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Update")
    public ResponseEntity<?> Update(VMMVariant data) {
        try {
            restTemplate.put(apiVariantUrl, data);
            ResponseEntity<VMMVariant> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + data.getId(), VMMVariant.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/Delete/{id}")
    public ModelAndView Delete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("variant/delete");
        try {
            ResponseEntity<VMMVariant> apiVariantResponse = restTemplate.getForEntity(apiVariantUrl + "/" + id, VMMVariant.class);
            ResponseEntity<VMMCategory[]> apiCategoryResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            if(apiVariantResponse.getStatusCode() == HttpStatus.OK && apiCategoryResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Variant", apiVariantResponse.getBody());
                view.addObject("Categories", apiCategoryResponse.getBody());
            } else {
                if(apiVariantResponse.getStatusCode() != HttpStatus.OK){
                    throw new Exception(apiVariantResponse.getStatusCode().toString() + ":" + apiVariantResponse.getBody());
                } else {
                    throw new Exception(apiCategoryResponse.getStatusCode().toString() + ":" + Arrays.toString(apiCategoryResponse.getBody()));
                }
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Delete")
    public ResponseEntity<?> Delete(VMMVariant data) {
        try {
            restTemplate.delete(apiVariantUrl + "/" + data.getId() + "/" + data.getUpdateBy());
            return new ResponseEntity<>("Variant " + data.getName() +" berhasil dihapus", HttpStatus.OK);
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/IndexAll")
    public ModelAndView IndexAll() {
        ModelAndView view = new ModelAndView("variant/index-all");
        view.addObject("title", "Variant Terhapus");
        try {
            ResponseEntity<VMMVariant[]> apiResponse = restTemplate.getForEntity(apiVariantUrl + "/RecycleBin", VMMVariant[].class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                VMMVariant[] data = apiResponse.getBody();
                view.addObject("Success", true);
                view.addObject("Variants", data);
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Retrieve/{id}")
    public ModelAndView Retrieve(@PathVariable long id) {
        ModelAndView view = new ModelAndView("variant/retrieve");
        
        try {
            ResponseEntity<VMMVariant> apiResponse = restTemplate.getForEntity(apiVariantUrl + "/findById/" + id, VMMVariant.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Variant", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Retrieve")
    public ResponseEntity<?> Retrieve(VMMVariant data) {
        try {
            restTemplate.put(apiVariantUrl + "/Retrieve", data);
            ResponseEntity<VMMVariant> apiResponse = restTemplate.getForEntity(apiVariantUrl + "/" + data.getId(), VMMVariant.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/HardDelete/{id}")
    public ModelAndView HardDelete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("variant/hard-delete");
        
        try {
            ResponseEntity<VMMVariant> apiResponse = restTemplate.getForEntity(apiVariantUrl + "/findById/" + id, VMMVariant.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Variant", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

   
}
