package com.SimpangRaya.SimpangRayaOnlineClient.Controllers;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.SimpangRaya.SimpangRayaOnlineClient.ViewModels.VMMCategory;

@Controller
// url : http://localhost:8000/Category
@RequestMapping("/Category")
public class CategoryController {

    private final String apiCategoryUrl = "http://localhost:8080/apiCategory";
    private RestTemplate restTemplate = new RestTemplate(); 

    @GetMapping("")
    public ModelAndView Index() {
        ModelAndView view = new ModelAndView("category/index");
        // Tambahakan object title didalam view
        view.addObject("title", "Daftar Category");
        try {
            // mengirim request ke server aplikasi
            ResponseEntity<VMMCategory[]> apiResponse = restTemplate.getForEntity(apiCategoryUrl, VMMCategory[].class);
            // check apakah response success
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                // tambahkan data kedalam view
                VMMCategory[] data = apiResponse.getBody();
                view.addObject("Response", "Success"); // tambahkan flag untuk response success
                view.addObject("Categories", data);
            } else {
                // throw error
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            // tambhakan object kedalam view jika terjadi error
            view.addObject("Response", "Fail");
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/IndexAll")
    public ModelAndView IndexAll() {
        ModelAndView view = new ModelAndView("category/index-all");
        view.addObject("title", "Category Terhapus");
        try {
            ResponseEntity<VMMCategory[]> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/RecycleBin", VMMCategory[].class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                VMMCategory[] data = apiResponse.getBody();
                view.addObject("Response", "Success");
                view.addObject("Categories", data);
            } else {
                throw new RestClientException(apiResponse.getStatusCode().toString() + " : " + Arrays.toString(apiResponse.getBody()));
            }
        } catch (RestClientException e) {
            view.addObject("Response", "Fail");
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Create")
    public ModelAndView Create() {
        ModelAndView view = new ModelAndView("category/create");
        view.addObject("VMMCategory", new VMMCategory());
        return view;
    }

    @PostMapping("/Create")
    public ResponseEntity<?> Create(VMMCategory data) {
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.postForEntity(apiCategoryUrl, data, VMMCategory.class);
            return new ResponseEntity<>(apiResponse.getBody(), apiResponse.getStatusCode());
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ModelAndView Detail(@PathVariable long id) {
        ModelAndView view = new ModelAndView("category/detail");
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + id, VMMCategory.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Category", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @GetMapping("/Update/{id}")
    public ModelAndView Update(@PathVariable long id) {
        ModelAndView view = new ModelAndView("category/update");
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + id, VMMCategory.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Category", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Update")
    public ResponseEntity<?> Update(VMMCategory data) {
        try {
            restTemplate.put(apiCategoryUrl, data);
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + data.getId(), VMMCategory.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/Delete/{id}")
    public ModelAndView Delete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("category/delete");
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + id, VMMCategory.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Category", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Delete")
    public ResponseEntity<?> Delete(VMMCategory data) {
        try {
            restTemplate.delete(apiCategoryUrl + "/" + data.getId() + "/" + data.getUpdateBy());
            return new ResponseEntity<>("Category " + data.getName() +" berhasil dihapus", HttpStatus.OK);
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/Retrieve/{id}")
    public ModelAndView Retrieve(@PathVariable long id) {
        ModelAndView view = new ModelAndView("category/retrieve");
        
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/findById/" + id, VMMCategory.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Category", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/Retrieve")
    public ResponseEntity<?> Retrieve(VMMCategory data) {
        try {
            restTemplate.put(apiCategoryUrl + "/Retrieve", data);
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/" + data.getId(), VMMCategory.class);
            return apiResponse;
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/HardDelete/{id}")
    public ModelAndView HardDelete(@PathVariable long id) {
        ModelAndView view = new ModelAndView("category/hard-delete");
        
        try {
            ResponseEntity<VMMCategory> apiResponse = restTemplate.getForEntity(apiCategoryUrl + "/findById/" + id, VMMCategory.class);
            if(apiResponse.getStatusCode() == HttpStatus.OK){
                view.addObject("Success", true);
                view.addObject("Category", apiResponse.getBody());
            } else {
                throw new Exception(apiResponse.getStatusCode().toString() + ":" + apiResponse.getBody());
            }
        } catch (Exception e) {
            view.addObject("Success", false);
            view.addObject("ErrorMessage", e.getMessage());
        }
        return view;
    }

    @PostMapping("/HardDelete")
    public ResponseEntity<?> HardDelete(VMMCategory data) {
        try {
            restTemplate.delete(apiCategoryUrl + "/HardDelete/" + data.getId());
            return new ResponseEntity<>("Category " + data.getName() +" berhasil dihapus", HttpStatus.OK);
        } catch (RestClientException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
