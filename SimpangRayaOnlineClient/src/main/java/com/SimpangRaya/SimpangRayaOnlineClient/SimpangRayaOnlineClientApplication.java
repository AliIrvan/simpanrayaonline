package com.SimpangRaya.SimpangRayaOnlineClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpangRayaOnlineClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpangRayaOnlineClientApplication.class, args);
	}

}
