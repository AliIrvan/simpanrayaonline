function Close(){
    $("#btnCloseModal").click()
}

$("#formDeleteCategory").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Category/Delete",
            type : "post",
            // dataType : "json",
            data : $("#formDeleteCategory").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})