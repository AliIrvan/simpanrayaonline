function Close(){
    $("#btnCloseModal").click()
}

$("#formRetrieveCategory").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Category/Retrieve",
            type : "post",
            dataType : "json",
            data : $("#formRetrieveCategory").serialize(),
            success : function(response){
                alert("Category " + response.name +" berhasil diaktifkan")
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})