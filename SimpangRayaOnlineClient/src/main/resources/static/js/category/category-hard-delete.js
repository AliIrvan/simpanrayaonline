function Close(){
    $("#btnCloseModal").click()
}

$("#formHardDeleteCategory").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Category/HardDelete",
            type : "post",
            data : $("#formHardDeleteCategory").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})