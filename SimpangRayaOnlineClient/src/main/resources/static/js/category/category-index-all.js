$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Aktifkan Category")
    $("#ModalBody").load("/Category/Retrieve/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Permanent")
    $("#ModalBody").load("/Category/HardDelete/" + id)
    $("#Modal").modal("show")
})