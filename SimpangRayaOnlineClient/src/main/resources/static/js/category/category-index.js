$("#btn_create").click(function(){
    $("#ModalLabel").text("Tambah Category")
    $("#ModalBody").load("/Category/Create")
    $("#Modal").modal("show")
})

$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Detail Category")
    $("#ModalBody").load("/Category/" + id)
    $("#Modal").modal("show")
})

$(".btn-warning").click(function(){
    let id = this.value
    $("#ModalLabel").text("Edit Category")
    $("#ModalBody").load("/Category/Update/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Category")
    $("#ModalBody").load("/Category/Delete/" + id)
    $("#Modal").modal("show")
})






