function Close(){
    $("#btnCloseModal").click()
}

$("#formUpdateCategory").validate({
    errorClass : "text-danger",
    rules : {
        name : {
            required : true,
            minlength : 5,
            maxlength : 100
        }
    },
    messages : {
        name : {
            required : "Nama harus diisi",
            minlength : "Nama terdiri dari 5-100 karakter",
            maxlength : "Nama terdiri dari 5-100 karakter"
        }
    },
    submitHandler : function(form){
        $.ajax({
            url : "/Category/Update",
            type : "post",
            dataType : "json",
            data : $("#formUpdateCategory").serialize(),
            success : function(response){
                alert("Category " + response.name + " berhasil diubah!")
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})