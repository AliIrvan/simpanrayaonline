function Close() {
    $("#btnCloseModal").click()
}

$("#formUpdateProduct").validate({
    errorClass: "text-danger",
    rules: {
        name: {
            required: true,
            minlength: 5,
            maxlength: 100
        },
        categoryId: "required",
        variantId : "required"
    },
    messages: {
        name: {
            required: "Nama harus diisi",
            minlength: "Nama terdiri dari 5-100 karakter",
            maxlength: "Nama terdiri dari 5-100 karakter"
        },
        categoryId: "Category harus ditentukan",
        variantId: "Variant harus ditentukan"
    },
    submitHandler: function (form) {
        $.ajax({
            url: "/Product/Update",
            type: "post",
            dataType: "json",
            data: $("#formUpdateProduct").serialize(),
            success: function (response) {
                alert("Product " + response.name + " berhasil diubah!")
                location.reload();
            },
            error: function (errResponse) {
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})