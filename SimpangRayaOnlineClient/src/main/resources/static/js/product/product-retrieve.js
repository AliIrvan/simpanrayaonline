function Close(){
    $("#btnCloseModal").click()
}

$("#formRetrieveProduct").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Product/Retrieve",
            type : "post",
            dataType : "json",
            data : $("#formRetrieveProduct").serialize(),
            success : function(response){
                alert("Product " + response.name +" berhasil diaktifkan")
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})