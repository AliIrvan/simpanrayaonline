$("#btn_create").click(function(){
    $("#ModalLabel").text("Tambah Product")
    $("#ModalBody").load("/Product/Create")
    $("#Modal").modal("show")
})

$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Detail Product")
    $("#ModalBody").load("/Product/" + id)
    $("#Modal").modal("show")
})

$(".btn-warning").click(function(){
    let id = this.value
    $("#ModalLabel").text("Edit Product")
    $("#ModalBody").load("/Product/Update/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Product")
    $("#ModalBody").load("/Product/Delete/" + id)
    $("#Modal").modal("show")
})






