$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Aktifkan Product")
    $("#ModalBody").load("/Product/Retrieve/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Permanent")
    $("#ModalBody").load("/Product/HardDelete/" + id)
    $("#Modal").modal("show")
})