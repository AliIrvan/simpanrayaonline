function Close(){
    $("#btnCloseModal").click()
}

$("#formHardDeleteProduct").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Product/HardDelete",
            type : "post",
            data : $("#formHardDeleteProduct").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})