function Close(){
    $("#btnCloseModal").click()
}

$("#formNewProduct").validate({
    errorClass : "text-danger",
    rules : {
        name : {
            required : true,
            minlength : 5,
            maxlength : 100
        },
        categoryId : "required",
        variantId : "required"
    },
    messages : {
        name : {
            required : "Nama harus diisi",
            minlength : "Nama terdiri dari 5-100 karakter",
            maxlength : "Nama terdiri dari 5-100 karakter"
        },
        categoryId : "Category harus ditentukan",
        variantId : "Category harus ditentukan"
    },
    submitHandler : function(form){
        $.ajax({
            url : "/Product/Create",
            type : "post",
            dataType : "json",
            data : $("#formNewProduct").serialize(),
            success : function(response){
                alert("Product " + response.name + " berhasil ditambahkan!")
                location.reload();

            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})
