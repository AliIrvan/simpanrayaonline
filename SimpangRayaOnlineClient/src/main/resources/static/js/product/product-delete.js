function Close(){
    $("#btnCloseModal").click()
}

$("#formDeleteProduct").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Product/Delete",
            type : "post",
            // dataType : "json",
            data : $("#formDeleteProduct").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})