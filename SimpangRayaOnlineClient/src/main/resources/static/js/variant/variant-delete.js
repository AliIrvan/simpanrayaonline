function Close(){
    $("#btnCloseModal").click()
}

$("#formDeleteVariant").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Variant/Delete",
            type : "post",
            // dataType : "json",
            data : $("#formDeleteVariant").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})