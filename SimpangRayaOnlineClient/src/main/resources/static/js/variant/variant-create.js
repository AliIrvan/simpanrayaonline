function Close(){
    $("#btnCloseModal").click()
}

$("#formNewVariant").validate({
    errorClass : "text-danger",
    rules : {
        name : {
            required : true,
            minlength : 5,
            maxlength : 100
        },
        categoryId : "required"
    },
    messages : {
        name : {
            required : "Nama harus diisi",
            minlength : "Nama terdiri dari 5-100 karakter",
            maxlength : "Nama terdiri dari 5-100 karakter"
        },
        categoryId : "Category harus ditentukan"
    },
    submitHandler : function(form){
        $.ajax({
            url : "/Variant/Create",
            type : "post",
            dataType : "json",
            data : $("#formNewVariant").serialize(),
            success : function(response){
                alert("Variant " + response.name + " berhasil ditambahkan!")
                location.reload();

            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})
