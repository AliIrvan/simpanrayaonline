function Close(){
    $("#btnCloseModal").click()
}

$("#formHardDeleteVariant").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Variant/HardDelete",
            type : "post",
            data : $("#formHardDeleteVariant").serialize(),
            success : function(response){
                alert(response)
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})