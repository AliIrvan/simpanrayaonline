$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Aktifkan Variant")
    $("#ModalBody").load("/Variant/Retrieve/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Permanent")
    $("#ModalBody").load("/Variant/HardDelete/" + id)
    $("#Modal").modal("show")
})