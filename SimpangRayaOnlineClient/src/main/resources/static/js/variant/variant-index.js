$("#btn_create").click(function(){
    $("#ModalLabel").text("Tambah Variant")
    $("#ModalBody").load("/Variant/Create")
    $("#Modal").modal("show")
})

$(".btn-success").click(function(){
    let id = this.value
    $("#ModalLabel").text("Detail Variant")
    $("#ModalBody").load("/Variant/" + id)
    $("#Modal").modal("show")
})

$(".btn-warning").click(function(){
    let id = this.value
    $("#ModalLabel").text("Edit Variant")
    $("#ModalBody").load("/Variant/Update/" + id)
    $("#Modal").modal("show")
})

$(".btn-danger").click(function(){
    let id = this.value
    $("#ModalLabel").text("Hapus Variant")
    $("#ModalBody").load("/Variant/Delete/" + id)
    $("#Modal").modal("show")
})






