function Close(){
    $("#btnCloseModal").click()
}

$("#formRetrieveVariant").validate({
    submitHandler : function(form){
        $.ajax({
            url : "/Variant/Retrieve",
            type : "post",
            dataType : "json",
            data : $("#formRetrieveVariant").serialize(),
            success : function(response){
                alert("Variant " + response.name +" berhasil diaktifkan")
                location.reload();
            },
            error : function(errResponse){
                alert("Error : " + errResponse.responseText)
            }
        })
    }
})