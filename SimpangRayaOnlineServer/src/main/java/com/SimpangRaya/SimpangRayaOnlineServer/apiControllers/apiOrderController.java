package com.SimpangRaya.SimpangRayaOnlineServer.apiControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.TOrderHeader;
import com.SimpangRaya.SimpangRayaOnlineServer.Services.OrderService;

@RestController
@RequestMapping("apiOrder")
public class apiOrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("")
    public ResponseEntity<?> GetAllData(){
        try {
            List<TOrderHeader> data = orderService.GetAllData();
            if(data.isEmpty()){
                return new ResponseEntity<>("Order kosong, Customer belum melakukan Order", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> GetById(@PathVariable final long id){
        try {
            TOrderHeader data = orderService.GetById(id);
            if(data.getId() == 0){
                return new ResponseEntity<>("Order tidak ditemukan", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/GetByCustomer/{customerId}")
    public ResponseEntity<?> GetByCustomer(@PathVariable final long customerId){
        try {
            List<TOrderHeader> data = orderService.GetByCustomer(customerId);
            if(data.isEmpty()){
                return new ResponseEntity<>("order tidak ditemukan", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/GetByCode/{trxCode}")
    public ResponseEntity<?> GetByCode(@PathVariable final String trxCode){
        try {
            List<TOrderHeader> data = orderService.GetByCode(trxCode);
            if(data.isEmpty()){
                return new ResponseEntity<>("order tidak ditemukan", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> Create(@RequestBody TOrderHeader data){
        try {
            TOrderHeader newData = orderService.Create(data);
            if(newData.getId() == 0){
                return new ResponseEntity<>("Order telah tersedia,tidak dapat melakukan order", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(newData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> Update(@RequestBody TOrderHeader data){
        try {
            TOrderHeader updatedData = orderService.Update(data);
            if(updatedData.getId() == 0){
                return new ResponseEntity<>("Order tidak tersedia,tidak dapat merubah order", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(updatedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}/{userId}")
    public ResponseEntity<?> Delete(@PathVariable final long id, @PathVariable final int userId){
        try {
            TOrderHeader deletedData = orderService.Delete(id, userId);
            if(deletedData.getId() == 0){
                return new ResponseEntity<>("Order tidak tersedia,tidak dapat menghapus order", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(deletedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/HardDelete/{id}")
    public ResponseEntity<?> HardDelete(@PathVariable final long id){
        try {
            orderService.HardDelete(id);
            return new ResponseEntity<>("Data (ID = " + id + " ) telah dihapus secara permanen", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    





}
