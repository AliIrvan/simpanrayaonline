package com.SimpangRaya.SimpangRayaOnlineServer.apiControllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MProduct;
import com.SimpangRaya.SimpangRayaOnlineServer.Services.ProductService;

@RestController
@RequestMapping("apiProduct")

public class apiProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("")
    public ResponseEntity<?> GetAllData() {
        try {
            List<Map<String, Object>> data = productService.GetAllData();
            // jika data Product kosong :
            if (data.isEmpty()) {
                return new ResponseEntity<>("Product kosong, silahkan masukan data", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> GetById(@PathVariable final long id) {
        try {
            Map<String, Object> data = productService.GetById(id);
            // jika data Product tidak tersedia :
            if (data.isEmpty()) {
                return new ResponseEntity<>("Product tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> Create(@RequestBody MProduct data) {
        try {
            MProduct newData = productService.Create(data);
            // jika data Product tidak tersedia :
            if (newData.getId() == 0) {
                return new ResponseEntity<>("Product telah tersedia, Product tidak boleh sama", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(newData, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> Update(@RequestBody MProduct data) {
        try {
            MProduct updatedData = productService.Update(data);
            // Return data kosong => data Product tidak ditemukan
            if (updatedData.getId() == 0) {
                return new ResponseEntity<>("Product tidak tersedia, tidak dapat mengubah data", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(updatedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}/{userId}")
    public ResponseEntity<?> SoftDelete(@PathVariable final long id, @PathVariable int userId) {
        try {
            MProduct deletedData = productService.SoftDelete(id, userId);
            // Jika deletedData kosong => data Product tidak ditemukan
            if (deletedData.getId() == 0) {
                return new ResponseEntity<>("Product tidak ditemukan, tidak dapat menghapus data", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(deletedData, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/HardDelete/{id}")
    public ResponseEntity<?> HardDelete(@PathVariable final long id) {
        try {
            productService.HardDelete(id);
            return new ResponseEntity<>("Category telah dihapus secara permanent", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/RecycleBin")
    public ResponseEntity<?> RecycleBin() {
        try {
            List<Map<String, Object>> data = productService.GetDeleted();
            // jika data Product kosong :
            if (data.isEmpty()) {
                return new ResponseEntity<>("Product terhapus tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> findById(@PathVariable final long id) {
        try {
            Map<String, Object> data = productService.findById(id);
            // jika data Product tidak tersedia :
            if (data.isEmpty()) {
                return new ResponseEntity<>("Product tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/Retrieve")
    public ResponseEntity<?> Retrieve(@RequestBody MProduct data) {
        try {
            MProduct updatedData = productService.Retrieve(data);
            // Return data kosong => data Product tidak ditemukan
            if (updatedData.getId() == 0) {
                return new ResponseEntity<>("Product tidak tersedia, tidak dapat mengubah data", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(updatedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
