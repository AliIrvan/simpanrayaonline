package com.SimpangRaya.SimpangRayaOnlineServer.apiControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MCategory;
import com.SimpangRaya.SimpangRayaOnlineServer.Services.CategoryService;

@RestController
@RequestMapping("/apiCategory")

public class apiCategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("")    
    public ResponseEntity<?> GetAllData(){
        try {
            List<MCategory> data = categoryService.GetAllData();
            // jika data Category kosong :
            if(data.isEmpty()){
                return new ResponseEntity<>("Category kosong, silahkan masukan data", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/RecycleBin")    
    public ResponseEntity<?> RecycleBin(){
        try {
            List<MCategory> data = categoryService.GetDeleted();
            // jika data Category kosong :
            if(data.isEmpty()){
                return new ResponseEntity<>("Category terhapus tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> GetById(@PathVariable final long id){
        try {
            MCategory data = categoryService.GetById(id);
            // jika data Category tidak tersedia :
            if(data.getId() == 0){
                return new ResponseEntity<>("Category tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> findById(@PathVariable final long id){
        try {
            MCategory data = categoryService.findById(id);
            // jika data Category tidak tersedia :
            if(data.getId() == 0){
                return new ResponseEntity<>("Category tidak ditemukan", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> Create(@RequestBody MCategory data){
        try {
            MCategory newData = categoryService.Create(data);
            // jika data Category tidak tersedia :
            if(newData.getId() == 0){
                return new ResponseEntity<>("Category telah tersedia, Category tidak boleh sama", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(newData, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> Update(@RequestBody MCategory data){
        try {
            MCategory updatedData = categoryService.Update(data);
            // Return data kosong => data category tidak ditemukan
            if(updatedData.getId() == 0){
                return new ResponseEntity<>("Category tidak tersedia, tidak dapat mengubah data", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(updatedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/Retrieve")
    public ResponseEntity<?> Retrieve(@RequestBody MCategory data){
        try {
            MCategory updatedData = categoryService.Retrieve(data);
            // Return data kosong => data category tidak ditemukan
            if(updatedData.getId() == 0){
                return new ResponseEntity<>("Category tidak tersedia, tidak dapat mengubah data", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(updatedData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}/{userId}")
    public ResponseEntity<?> SoftDelete(@PathVariable final long id, @PathVariable int userId){
        try {
            MCategory deletedData = categoryService.SoftDelete(id, userId);
            // Jika deletedData kosong => data Category tidak ditemukan
            if(deletedData.getId() == 0){
                return new ResponseEntity<>("Category tidak ditemukan, tidak dapat menghapus data", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(deletedData, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/HardDelete/{id}")
    public ResponseEntity<?> HardDelete(@PathVariable final long id){
        try {
            categoryService.HardDelete(id);
            return new ResponseEntity<>("Category telah dihapus secara permanent", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
