package com.SimpangRaya.SimpangRayaOnlineServer.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MVariant;

@Repository
public interface VariantRepository extends JpaRepository<MVariant, Long> {

    @Query(value = "SELECT v.id, v.name, v.category_id \"categoryId\", c.name \"categoryName\", v.description, " + 
                "v.is_deleted \"deleted\", v.create_by \"createBy\", v.create_date \"createDate\",  " + 
                "v.update_by \"updateBy\", v.update_date \"updateDate\" " + 
                "FROM Tbl_M_Variant AS v " + 
                "JOIN Tbl_M_Category AS c " + 
                "ON v.category_id = c.id " + 
                "WHERE v.is_deleted = false ORDER BY v.id ASC", nativeQuery = true)
    Optional<List<Map<String, Object>>> GetAllActiveData();

    @Query(value = "SELECT v.id, v.name, v.category_id \"categoryId\", c.name \"categoryName\", v.description, " + 
                "v.is_deleted \"deleted\", v.create_by \"createBy\", v.create_date \"createDate\",  " + 
                "v.update_by \"updateBy\", v.update_date \"updateDate\" " + 
                "FROM Tbl_M_Variant AS v " + 
                "JOIN Tbl_M_Category AS c " + 
                "ON v.category_id = c.id " + 
                "WHERE v.is_deleted = false AND v.id = :id", nativeQuery = true)
    Optional<Map<String, Object>> GetActiveDatabyId(@Param("id") long id);

    
    @Query(value = "SELECT * FROM Tbl_M_Variant WHERE is_deleted = false AND name = :name", nativeQuery = true)
    Optional<MVariant> GetActicveDatabyName(@Param("name") String name);

    @Query(value = "SELECT * FROM Tbl_M_Variant WHERE is_deleted = false AND category_id = :categoryId ORDER BY id ASC", nativeQuery = true)
    Optional<List<MVariant>> GetAllActiveDataByCategoryId(@Param("categoryId") long categoryId);

    @Query(value = "SELECT * FROM Tbl_M_Variant WHERE is_deleted = false AND id = :id", nativeQuery = true)
    Optional<MVariant> GetActiveVariantbyId(@Param("id")long id);

    @Query(value = "SELECT v.id, v.name, v.category_id \"categoryId\", c.name \"categoryName\", v.description, " + 
                "v.is_deleted \"deleted\", v.create_by \"createBy\", v.create_date \"createDate\",  " + 
                "v.update_by \"updateBy\", v.update_date \"updateDate\" " + 
                "FROM Tbl_M_Variant AS v " + 
                "JOIN Tbl_M_Category AS c " + 
                "ON v.category_id = c.id " + 
                "WHERE v.id=:id ORDER BY v.id ASC", nativeQuery = true)
    Optional<Map<String, Object>> GetAllDataById(@Param("id") long id);

    @Query(value = "SELECT v.id, v.name, v.category_id \"categoryId\", c.name \"categoryName\", v.description, " + 
                "v.is_deleted \"deleted\", v.create_by \"createBy\", v.create_date \"createDate\",  " + 
                "v.update_by \"updateBy\", v.update_date \"updateDate\" " + 
                "FROM Tbl_M_Variant AS v " + 
                "JOIN Tbl_M_Category AS c " + 
                "ON v.category_id = c.id " + 
                "WHERE v.is_deleted = true ORDER BY v.id ASC", nativeQuery = true)
    Optional<List<Map<String, Object>>> GetAllDeleted();

}
