package com.SimpangRaya.SimpangRayaOnlineServer.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MProduct;

@Repository
public interface ProductRepository extends JpaRepository<MProduct, Long> {

    @Query(value = "SELECT p.id, p.name, v.category_id \"categoryId\", c.name \"categoryName\", p.variant_id \"variantId\", v.name \"variantName\", "
            + "p.description, p.stock, p.price, p.is_deleted \"deleted\", p.create_by \"createBy\", p.create_date \"createDate\",  "
            + "p.update_by \"updateBy\", p.update_date \"updateDate\" "
            + "FROM Tbl_M_Product AS p "
            + "JOIN Tbl_M_Variant as v "
            + "ON p.variant_id = v.id "
            + "JOIN Tbl_M_Category AS c "
            + "ON v.category_id = c.id "
            + "WHERE p.is_deleted = false", nativeQuery = true)
    Optional<List<Map<String, Object>>> GetAllActiveData();

    @Query(value = "SELECT p.id, p.name, v.category_id \"categoryId\", c.name \"categoryName\", p.variant_id \"variantId\", v.name \"variantName\", "
            + "p.description, p.stock, p.price, p.is_deleted \"deleted\", p.create_by \"createBy\", p.create_date \"createDate\",  "
            + "p.update_by \"updateBy\", p.update_date \"updateDate\" "
            + "FROM Tbl_M_Product AS p "
            + "JOIN Tbl_M_Variant as v "
            + "ON p.variant_id = v.id "
            + "JOIN Tbl_M_Category AS c "
            + "ON v.category_id = c.id "
            + "WHERE p.is_deleted = false AND p.id = :id", nativeQuery = true)
    Optional<Map<String, Object>> GetActiveDatabyId(@Param("id") long id);

    @Query(value = "SELECT * FROM Tbl_M_Product WHERE is_deleted = false AND name = :name", nativeQuery = true)
    Optional<MProduct> GetActicveDatabyName(@Param("name") String name);

    @Query(value = "SELECT * FROM Tbl_M_Product WHERE is_deleted = false AND category_id = :categoryId ORDER BY id ASC", nativeQuery = true)
    Optional<List<MProduct>> GetAllActiveDataByCategoryId(@Param("categoryId") long categoryId);

    @Query(value = "SELECT * FROM Tbl_M_Product WHERE is_deleted = false AND variant_id = :variantId ORDER BY id ASC", nativeQuery = true)
    Optional<List<MProduct>> GetAllActiveDataByVariantId(@Param("variantId") long variantId);

    @Query(value = "SELECT * FROM Tbl_M_Product WHERE is_deleted = false AND id = :id", nativeQuery = true)
    Optional<MProduct> GetActiveProductbyId(@Param("id") long id);

    @Query(value = "SELECT name FROM Tbl_M_Product WHERE id = :productId", nativeQuery = true)
    String GetNameById(@Param("productId") long productId);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Tbl_M_Product SET stock = :currentStock WHERE id = :id ", nativeQuery = true)
    void UpdateStock(@Param("id") long id, @Param("currentStock") int currentStock);

    @Query(value = "SELECT p.id, p.name, v.category_id \"categoryId\", c.name \"categoryName\", p.variant_id \"variantId\", v.name \"variantName\", "
            + "p.description, p.stock, p.price, p.is_deleted \"deleted\", p.create_by \"createBy\", p.create_date \"createDate\",  "
            + "p.update_by \"updateBy\", p.update_date \"updateDate\" "
            + "FROM Tbl_M_Product AS p "
            + "JOIN Tbl_M_Variant as v "
            + "ON p.variant_id = v.id "
            + "JOIN Tbl_M_Category AS c "
            + "ON v.category_id = c.id "
            + "WHERE p.is_deleted = true ORDER BY p.id ASC", nativeQuery = true)
    Optional<List<Map<String, Object>>> GetAllDeleted();

    @Query(value = "SELECT p.id, p.name, v.category_id \"categoryId\", c.name \"categoryName\", p.variant_id \"variantId\", v.name \"variantName\", "
            + "p.description, p.stock, p.price, p.is_deleted \"deleted\", p.create_by \"createBy\", p.create_date \"createDate\",  "
            + "p.update_by \"updateBy\", p.update_date \"updateDate\" "
            + "FROM Tbl_M_Product AS p "
            + "JOIN Tbl_M_Variant as v "
            + "ON p.variant_id = v.id "
            + "JOIN Tbl_M_Category AS c "
            + "ON v.category_id = c.id "
            + "WHERE p.id= :id ORDER BY p.id ASC", nativeQuery = true)
    Optional<Map<String, Object>> GetAllDataById(@Param("id") long id);

}
