package com.SimpangRaya.SimpangRayaOnlineServer.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.TOrderDetail;

@Repository
public interface OrderDetailRepository extends JpaRepository<TOrderDetail, Long>{

}
