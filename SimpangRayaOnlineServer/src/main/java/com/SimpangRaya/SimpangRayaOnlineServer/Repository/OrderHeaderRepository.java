package com.SimpangRaya.SimpangRayaOnlineServer.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.TOrderHeader;

@Repository
public interface OrderHeaderRepository extends JpaRepository<TOrderHeader, Long>{
    Optional<List<TOrderHeader>> findByDeleted(boolean deleted);
    Optional<List<TOrderHeader>> findByCustomerIdAndDeleted(long customerId, boolean deleted);
    Optional<List<TOrderHeader>> findByTrxCodeAndDeleted(String trxCode, boolean deleted);
    Optional<TOrderHeader> findByIdAndDeleted(long id, boolean deleted);
}
