package com.SimpangRaya.SimpangRayaOnlineServer.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MCategory;

@Repository
public interface CategoryRepository extends JpaRepository<MCategory, Long>{

    @Query(value="SELECT * FROM Tbl_M_Category WHERE is_deleted = false ORDER BY id ASC", nativeQuery=true)
    Optional<List<MCategory>> GetAllActiveData();

    @Query(value="SELECT * FROM Tbl_M_Category ORDER BY is_deleted, id ASC", nativeQuery=true)
    Optional<List<MCategory>> GetAllData();
    
    @Query(value="SELECT * FROM Tbl_M_Category WHERE is_deleted = false AND id = :id", nativeQuery=true)
    Optional<MCategory> GetActicveDatabyId(@Param("id") long id);

    @Query(value="SELECT * FROM Tbl_M_Category WHERE is_deleted = false AND name = :name", nativeQuery=true)
    Optional<MCategory> GetActicveDatabyName(@Param("name") String name);

    Optional<List<MCategory>> findByDeleted(boolean deleted);

}
