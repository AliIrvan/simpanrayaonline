package com.SimpangRaya.SimpangRayaOnlineServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpangRayaOnlineServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpangRayaOnlineServerApplication.class, args);
	}

}
