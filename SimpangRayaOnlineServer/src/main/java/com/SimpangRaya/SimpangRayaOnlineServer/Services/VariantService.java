package com.SimpangRaya.SimpangRayaOnlineServer.Services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MVariant;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.VariantRepository;

import jakarta.transaction.Transactional;

@Transactional
@Service
public class VariantService {

    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private CategoryService categoryService;

    public List<Map<String, Object>> GetAllData() {
        return variantRepository.GetAllActiveData().orElse(null);
    }

    public List<MVariant> GetAllDataByCategotyId(long categoryId) {
        return variantRepository.GetAllActiveDataByCategoryId(categoryId).orElse(null);
    }

    public Map<String, Object> GetById(long id) {
        return variantRepository.GetActiveDatabyId(id).orElse(new HashMap<>());
    }

    public MVariant Create(MVariant data) throws Exception {
        try {
            // check apakah data telah tersedia berdasarkan nama
            if (variantRepository.GetActicveDatabyName(data.getName()).isPresent()) {
                return new MVariant();
            } else {
                // check apakah Category tersedia berdasarkan categoryId
                if(categoryService.GetById(data.getCategoryId()).getId() == 0){
                    throw new Exception("Category tidak tersedia, silahkan masukan data Category");
                }
                return variantRepository.save(data);
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MVariant Update(MVariant data) throws Exception {
        try {
            // check apakah Category tersedia berdasarkan id
            Optional<MVariant> existVariant = variantRepository.GetActiveVariantbyId(data.getId());
            if (existVariant.isEmpty()) {
                return new MVariant();
            } else {
                if(variantRepository.GetActicveDatabyName(data.getName()).isPresent()){
                    throw new Exception("Variant telah tersedia, tidak dapat mengubah data");
                }
                data.setCreateBy(existVariant.get().getCreateBy());
                data.setCreateDate(existVariant.get().getCreateDate());
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(false);
                return variantRepository.save(data);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MVariant SoftDelete(long id, int userId) {
        try {
            // Check apakah Category yang akan dihapus tersedia berdasarkan ID
            Optional<MVariant> existVariant = variantRepository.GetActiveVariantbyId(id);
            if (existVariant.isEmpty()) {
                return new MVariant();
            } else {
                MVariant data = existVariant.get();
                data.setUpdateBy(userId);
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(true);
                return variantRepository.save(data);
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public void HardDelete(long id) throws Exception {
        try {
            if(!variantRepository.existsById(id)){
                throw new Exception("Variant tidak tersedia, tidak dapat menghapus data");
            }
            variantRepository.deleteById(id);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public boolean CheckVariantisExist(long id){
        return variantRepository.existsById(id);
    }

    public Map<String, Object> findById(long id) {
        return variantRepository.GetAllDataById(id).orElse(new HashMap<>());
    }

    public List<Map<String, Object>> GetDeleted() {
        return variantRepository.GetAllDeleted().orElse(new ArrayList<>());
    }

    public MVariant Retrieve(MVariant data) {
        try {
            // check apakah Category tersedia berdasarkan id
            Optional<MVariant> existVariant = variantRepository.findById(data.getId());
            if(existVariant.isEmpty()){
                return new MVariant();
            } else {
                MVariant  retrieveData = existVariant.get();
                retrieveData.setUpdateBy(data.getUpdateBy());
                retrieveData.setUpdateDate(LocalDateTime.now());
                retrieveData.setDeleted(false);
                return variantRepository.save(retrieveData);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

}
