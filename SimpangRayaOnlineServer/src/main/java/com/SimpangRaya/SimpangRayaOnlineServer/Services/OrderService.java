package com.SimpangRaya.SimpangRayaOnlineServer.Services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MProduct;
import com.SimpangRaya.SimpangRayaOnlineServer.Models.TOrderDetail;
import com.SimpangRaya.SimpangRayaOnlineServer.Models.TOrderHeader;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.OrderDetailRepository;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.OrderHeaderRepository;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.ProductRepository;

@Transactional
@Service
public class OrderService {

    @Autowired
    private OrderHeaderRepository orderHeaderRepository;
    
    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    public List<TOrderHeader> GetAllData() {
        return orderHeaderRepository.findByDeleted(false).orElse(new ArrayList<>());
    }

    public TOrderHeader GetById(long id) {
        return orderHeaderRepository.findByIdAndDeleted(id, false).orElse(new TOrderHeader());
    }

    public List<TOrderHeader> GetByCustomer(long customerId) {
        return orderHeaderRepository.findByCustomerIdAndDeleted(customerId, false).orElse(new ArrayList<>());
    }


    public List<TOrderHeader> GetByCode(String trxCode) {
        return orderHeaderRepository.findByTrxCodeAndDeleted(trxCode, false).orElse(new ArrayList<>());
    }

    public TOrderHeader Create(TOrderHeader data) throws Exception {
        try {
            // Check apakah order telah tersedia berdasarkan id
            Optional<TOrderHeader> existOrder = orderHeaderRepository.findById(data.getId());
            // Jika Order ditemukan, return TorderHeader kosong
            if(existOrder.isPresent()){ 
                return new TOrderHeader();
            }
            // Simpan data OrderHeader
            orderHeaderRepository.save(data);
            // Simpan data OrderDetail : Check apakah OrderDetail ada
            if(data.getOrderDetails().isEmpty()){
                throw new Exception("Detail pesanan tidak ditemukan, tidak dapat melakukan order");
            }
            for (TOrderDetail orderDetails : data.getOrderDetails()) {
                orderDetails.setOrderHeaderId(data.getId());
                orderDetails.setCreateBy(data.getCreateBy());
                orderDetails.setCreateDate(data.getCreateDate());
                
                // Update Stock : Check apakah product tersedia
                Optional<MProduct> existProduct = productRepository.findById(orderDetails.getProductId());
                if(existProduct.isEmpty()){ // jika product tidak tersedia Throw Error
                    throw new Exception(productRepository.GetNameById(orderDetails.getProductId()) + " tidak tersedia, tidak dapat melakukan order");
                } else {
                    // Check apakah Stock tersedia
                    int currentStock = existProduct.get().getStock();
                    if(currentStock < orderDetails.getQty()){ // Jika Stock tidak cukup
                        throw new Exception("Stock tidak mencukupi, tidak dapat melakukan order");
                    } else {
                        // UpdateStock
                        currentStock -= orderDetails.getQty();
                        productRepository.UpdateStock(orderDetails.getProductId(), currentStock);
                    }
                }
                orderDetailRepository.save(orderDetails);
            }
            return data;  
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public TOrderHeader Update(TOrderHeader data) throws Exception {
        try {
            // Check apakah order telah tersedia berdasarkan id
            Optional<TOrderHeader> existOrder = orderHeaderRepository.findById(data.getId());
            // Jika Order ditemukan, return TorderHeader kosong
            if(existOrder.isEmpty()){ 
                return new TOrderHeader();
            }
            // Simpan data OrderHeader
            data.setUpdateDate(LocalDateTime.now());
            data.setCreateBy(existOrder.get().getCreateBy());
            data.setCreateDate(existOrder.get().getCreateDate());
            orderHeaderRepository.save(data);
            // Simpan data OrderDetail : Check apakah OrderDetail ada
            if(data.getOrderDetails().isEmpty()){
            throw new Exception("Detail pesanan tidak ditemukan, tidak dapat mengubah order");
            }
            for (TOrderDetail orderDetails : data.getOrderDetails()) {
                TOrderDetail existOrderDetails = orderDetailRepository.findById(data.getId()).get();
                existOrderDetails.setCreateBy(data.getCreateBy());
                existOrderDetails.setCreateDate(data.getCreateDate());
                existOrderDetails.setCreateDate(LocalDateTime.now());
                
                // Update Stock : Check apakah product tersedia
                Optional<MProduct> existProduct = productRepository.findById(orderDetails.getProductId());
                if(existProduct.isEmpty()){ // jika product tidak tersedia Throw Error
                    throw new Exception(productRepository.GetNameById(orderDetails.getProductId()) + " tidak tersedia, tidak dapat mengubah order");
                } else {
                    // Check Jumlah Stock tersedia
                    int currentStock = existProduct.get().getStock();
                    // jika jumlah qty ditambah 
                    if(orderDetails.getQty() > existOrderDetails.getQty()){
                        currentStock -= orderDetails.getQty() - existOrderDetails.getQty();
                    } else { // jika jumlah qty di kurangi
                        currentStock += existOrderDetails.getQty() - orderDetails.getQty();
                    }

                    if(currentStock <= 0){
                        throw new Exception("Stock habis, tidak dapat mengubah pesanan");
                    }
                    productRepository.UpdateStock(orderDetails.getProductId(), currentStock);
                }
                orderDetailRepository.save(existOrderDetails);
            }
            return data; 
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public TOrderHeader Delete(long id, int userId) throws Exception {
        try {
            // Check apakah order telah tersedia berdasarkan id
            Optional<TOrderHeader> existOrder = orderHeaderRepository.findById(id);
            // Jika Order ditemukan, return TorderHeader kosong
            if(existOrder.isEmpty()){ 
                return new TOrderHeader();
            }
            // SoftDelete data OrderHeader
            TOrderHeader data = existOrder.get();
            data.setUpdateDate(LocalDateTime.now());
            data.setUpdateBy(userId);
            data.setDeleted(true);
            orderHeaderRepository.save(data);
            // Simpan data OrderDetail : Check apakah OrderDetail ada
            if(data.getOrderDetails().isEmpty()){
                throw new Exception("Detail pesanan tidak ditemukan, tidak dapat menghapus order");
            }
            for (TOrderDetail orderDetails : data.getOrderDetails()) {
                TOrderDetail existOrderDetail = orderDetailRepository.findById(data.getId()).get(); 
                existOrderDetail.setDeleted(true);
                existOrderDetail.setUpdateBy(userId);
                existOrderDetail.setUpdateDate(data.getUpdateDate());

                // Update Stock : Check apakah product tersedia
                Optional<MProduct> existProduct = productRepository.findById(orderDetails.getProductId());
                if(existProduct.isEmpty()){ // jika product tidak tersedia Throw Error
                    throw new Exception(productRepository.GetNameById(orderDetails.getProductId()) + " tidak tersedia, tidak dapat menghapus order");
                } else {
                    // Check apakah Stock tersedia
                    int currentStock = existProduct.get().getStock() + orderDetails.getQty();
                    productRepository.UpdateStock(orderDetails.getProductId(), currentStock);
                    
                }
                orderDetailRepository.save(orderDetails);
            }
            return data;  
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public void HardDelete(long id) throws Exception {
        try {
            Optional<TOrderHeader> data = orderHeaderRepository.findById(id);
            if(data.isEmpty()){
                throw new Exception("Order tidak ditemukan, tidak dapat menghapus data");
            } else {
                for (TOrderDetail orderDetail : data.get().getOrderDetails()) {
                    orderDetailRepository.deleteById(data.get().getId());
                }
                orderHeaderRepository.deleteById(id);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

}
