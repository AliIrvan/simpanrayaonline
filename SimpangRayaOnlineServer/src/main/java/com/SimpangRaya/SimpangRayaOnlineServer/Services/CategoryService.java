package com.SimpangRaya.SimpangRayaOnlineServer.Services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MCategory;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.CategoryRepository;
@Transactional
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<MCategory> GetAllData() {
        return categoryRepository.GetAllActiveData().orElse(null);
    }

    public MCategory GetById(long id) {
        return categoryRepository.GetActicveDatabyId(id).orElse(new MCategory());
    }

    public MCategory Create(MCategory data) {
        try {
            // check apakah data telah tersedia berdasarkan nama
            if (categoryRepository.GetActicveDatabyName(data.getName()).isPresent()) {
                return new MCategory();
            }
            return categoryRepository.save(data);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MCategory Update(MCategory data) throws Exception {
        try {
            // check apakah Category tersedia berdasarkan id
            Optional<MCategory> existCategory = categoryRepository.GetActicveDatabyId(data.getId());
            if(existCategory.isEmpty()){
                return new MCategory();
            } else {
                if(data.getName().equals(existCategory.get().getName())){
                    throw new Exception("Nama Category telah tersedia,tidak dapat mengubah data");
                }
                data.setCreateBy(existCategory.get().getCreateBy());
                data.setCreateDate(existCategory.get().getCreateDate());
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(false);
                return categoryRepository.save(data);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MCategory SoftDelete(long id, int userId) {
        try {
            // Check apakah Category yang akan dihapus tersedia berdasarkan ID
            Optional<MCategory> existCategory = categoryRepository.GetActicveDatabyId(id);
            if(existCategory.isEmpty()){
                return new MCategory();
            } else {
                MCategory data = existCategory.get();
                data.setUpdateBy(userId);
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(true);
                return categoryRepository.save(data);
            }
            
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw  e;
        }
    }

    public void HardDelete(long id) throws Exception {
        try {
            if(!categoryRepository.existsById(id)){
                throw new Exception("Category tidak tersedia, tidak dapat menghapus data");
            }
            categoryRepository.deleteById(id);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public List<MCategory> GetDeleted() {
        return categoryRepository.findByDeleted(true).orElse(new ArrayList<>());
    }

    public MCategory findById(long id) {
        return categoryRepository.findById(id).orElse(new MCategory());
    }

    public MCategory Retrieve(MCategory data) throws Exception {
        try {
            // check apakah Category tersedia berdasarkan id
            Optional<MCategory> existCategory = categoryRepository.findById(data.getId());
            if(existCategory.isEmpty()){
                return new MCategory();
            } else {
                MCategory  retrieveData = existCategory.get();
                retrieveData.setUpdateBy(data.getUpdateBy());
                retrieveData.setUpdateDate(LocalDateTime.now());
                retrieveData.setDeleted(false);
                return categoryRepository.save(retrieveData);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

}
