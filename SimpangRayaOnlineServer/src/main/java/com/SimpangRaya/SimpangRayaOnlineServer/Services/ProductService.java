package com.SimpangRaya.SimpangRayaOnlineServer.Services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.SimpangRaya.SimpangRayaOnlineServer.Models.MProduct;
import com.SimpangRaya.SimpangRayaOnlineServer.Repository.ProductRepository;



@Transactional
@Service
public class ProductService {


    @Autowired
    private VariantService variantService;

    @Autowired
    private ProductRepository productRepository;

    public List<Map<String, Object>> GetAllData() {
        return productRepository.GetAllActiveData().orElse(null);
    }

    public List<MProduct> GetAllDataByVariantId(long variantId) {
        return productRepository.GetAllActiveDataByVariantId(variantId).orElse(null);
    }

    public Map<String, Object> GetById(long id) {
        return productRepository.GetActiveDatabyId(id).orElse(new HashMap<>());
    }

    public MProduct Create(MProduct data) throws Exception {
        try {
            // check apakah data telah tersedia berdasarkan nama
            if (productRepository.GetActicveDatabyName(data.getName()).isPresent()) {
                return new MProduct();
            } else {
                // check apakah Variant tersedia berdasarkan variantId
                if (variantService.GetById(data.getVariantId()).isEmpty()) {
                    throw new Exception("Variant tidak tersedia, silahkan masukan data Variant");
                }
                return productRepository.save(data);
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MProduct Update(MProduct data) throws Exception {
        try {
            // check apakah Product tersedia berdasarkan id
            Optional<MProduct> existProduct = productRepository.GetActiveProductbyId(data.getId());
            if (existProduct.isEmpty()) {
                return new MProduct();
            } else {
                if(!variantService.CheckVariantisExist(data.getVariantId())){
                    throw new Exception("Variant ridak tersedia, tidak dapat menyimpan data");
                }
                data.setCreateBy(existProduct.get().getCreateBy());
                data.setCreateDate(existProduct.get().getCreateDate());
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(false);
                return productRepository.save(data);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public MProduct SoftDelete(long id, int userId) {
        try {
            // Check apakah Category yang akan dihapus tersedia berdasarkan ID
            Optional<MProduct> existCategory = productRepository.GetActiveProductbyId(id);
            if (existCategory.isEmpty()) {
                return new MProduct();
            } else {
                MProduct data = existCategory.get();
                data.setUpdateBy(userId);
                data.setUpdateDate(LocalDateTime.now());
                data.setDeleted(true);
                return productRepository.save(data);
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public void HardDelete(long id) throws Exception {
        try {
            if (!productRepository.existsById(id)) {
                throw new Exception("product tidak tersedia, tidak dapat menghapus data");
            }
            productRepository.deleteById(id);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

    public Map<String, Object> findById(long id) {
        return productRepository.GetAllDataById(id).orElse(new HashMap<>());

    }

    public List<Map<String, Object>> GetDeleted() {
        return productRepository.GetAllDeleted().orElse(new ArrayList<>());
    }

    public MProduct Retrieve(MProduct data) {
        try {
            // check apakah Category tersedia berdasarkan id
            Optional<MProduct> existProduct = productRepository.findById(data.getId());
            if(existProduct.isEmpty()){
                return new MProduct();
            } else {
                MProduct  retrieveData = existProduct.get();
                retrieveData.setUpdateBy(data.getUpdateBy());
                retrieveData.setUpdateDate(LocalDateTime.now());
                retrieveData.setDeleted(false);
                return productRepository.save(retrieveData);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }
}
